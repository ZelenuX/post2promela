package su.nsk.iae.post.promela;

import org.eclipse.xtend2.lib.StringConcatenation;

public interface PromelaElement {
    StringConcatenation toText();
}
