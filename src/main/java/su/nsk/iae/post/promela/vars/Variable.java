package su.nsk.iae.post.promela.vars;

import org.eclipse.xtend2.lib.StringConcatenation;
import su.nsk.iae.post.promela.PromelaElement;

public class Variable implements PromelaElement {
    private boolean constant = false;
    private VarType type;
    private String name;

    public Variable(VarType type, String name, boolean constant) {
        this.constant = constant;
        this.type = type;
        this.name = name;
    }

    public Variable(VarType type, String name) {
        this.type = type;
        this.name = name;
    }

    @Override
    public StringConcatenation toText() {
        if (constant) {
            return new StringConcatenation("#define " + name);
        }
        else {
            return new StringConcatenation(type.toString() + " " + name + ";");
        }
    }
}
