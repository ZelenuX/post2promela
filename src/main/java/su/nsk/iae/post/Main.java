package su.nsk.iae.post;

import com.google.inject.Injector;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import su.nsk.iae.post.postCore.PoSTStandaloneSetup;
import su.nsk.iae.post.postCore.poST.Model;
import su.nsk.iae.post.promela.Program;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Hello!");

        Model m = prepareAndParseModelFromResource("/Sanitizer.post");
        System.out.println("Program name: " + m.getPrograms().get(0).getName());

        System.out.println(buildProgram(m).toText().toString());

        System.out.println("Bye!");
    }

    private static Program buildProgram(Model m) {
        return new Program();
    }

    private static Model prepareAndParseModelFromResource(String res) throws IOException {
        Injector i = new PoSTStandaloneSetup().createInjectorAndDoEMFRegistration();
        ResourceSet rs = i.getInstance(ResourceSet.class);

        Resource r = rs.getResource(URI.createFileURI(Main.class.getResource(res).getFile()), true);
        r.load(null);
        return  (Model)r.getContents().get(0);
    }
}
