/**
 * generated by Xtext 2.25.0
 */
package su.nsk.iae.post.postCore.poST;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Process#getProcInVars <em>Proc In Vars</em>}</li>
 *   <li>{@link Process#getProcOutVars <em>Proc Out Vars</em>}</li>
 *   <li>{@link Process#getProcInOutVars <em>Proc In Out Vars</em>}</li>
 *   <li>{@link Process#getProcProcessVars <em>Proc Process Vars</em>}</li>
 *   <li>{@link Process#getProcVars <em>Proc Vars</em>}</li>
 *   <li>{@link Process#getProcTempVars <em>Proc Temp Vars</em>}</li>
 *   <li>{@link Process#getStates <em>States</em>}</li>
 * </ul>
 *
 * @see PoSTPackage#getProcess()
 * @model
 * @generated
 */
public interface Process extends Variable
{
  /**
   * Returns the value of the '<em><b>Proc In Vars</b></em>' containment reference list.
   * The list contents are of type {@link InputVarDeclaration}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Proc In Vars</em>' containment reference list.
   * @see PoSTPackage#getProcess_ProcInVars()
   * @model containment="true"
   * @generated
   */
  EList<InputVarDeclaration> getProcInVars();

  /**
   * Returns the value of the '<em><b>Proc Out Vars</b></em>' containment reference list.
   * The list contents are of type {@link OutputVarDeclaration}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Proc Out Vars</em>' containment reference list.
   * @see PoSTPackage#getProcess_ProcOutVars()
   * @model containment="true"
   * @generated
   */
  EList<OutputVarDeclaration> getProcOutVars();

  /**
   * Returns the value of the '<em><b>Proc In Out Vars</b></em>' containment reference list.
   * The list contents are of type {@link InputOutputVarDeclaration}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Proc In Out Vars</em>' containment reference list.
   * @see PoSTPackage#getProcess_ProcInOutVars()
   * @model containment="true"
   * @generated
   */
  EList<InputOutputVarDeclaration> getProcInOutVars();

  /**
   * Returns the value of the '<em><b>Proc Process Vars</b></em>' containment reference list.
   * The list contents are of type {@link ProcessVarDeclaration}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Proc Process Vars</em>' containment reference list.
   * @see PoSTPackage#getProcess_ProcProcessVars()
   * @model containment="true"
   * @generated
   */
  EList<ProcessVarDeclaration> getProcProcessVars();

  /**
   * Returns the value of the '<em><b>Proc Vars</b></em>' containment reference list.
   * The list contents are of type {@link VarDeclaration}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Proc Vars</em>' containment reference list.
   * @see PoSTPackage#getProcess_ProcVars()
   * @model containment="true"
   * @generated
   */
  EList<VarDeclaration> getProcVars();

  /**
   * Returns the value of the '<em><b>Proc Temp Vars</b></em>' containment reference list.
   * The list contents are of type {@link TempVarDeclaration}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Proc Temp Vars</em>' containment reference list.
   * @see PoSTPackage#getProcess_ProcTempVars()
   * @model containment="true"
   * @generated
   */
  EList<TempVarDeclaration> getProcTempVars();

  /**
   * Returns the value of the '<em><b>States</b></em>' containment reference list.
   * The list contents are of type {@link State}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>States</em>' containment reference list.
   * @see PoSTPackage#getProcess_States()
   * @model containment="true"
   * @generated
   */
  EList<State> getStates();

} // Process
