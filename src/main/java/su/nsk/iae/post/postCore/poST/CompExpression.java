/**
 * generated by Xtext 2.25.0
 */
package su.nsk.iae.post.postCore.poST;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comp Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link CompExpression#getCompOp <em>Comp Op</em>}</li>
 * </ul>
 *
 * @see PoSTPackage#getCompExpression()
 * @model
 * @generated
 */
public interface CompExpression extends AndExpression
{
  /**
   * Returns the value of the '<em><b>Comp Op</b></em>' attribute.
   * The literals are from the enumeration {@link CompOperator}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Comp Op</em>' attribute.
   * @see CompOperator
   * @see #setCompOp(CompOperator)
   * @see PoSTPackage#getCompExpression_CompOp()
   * @model
   * @generated
   */
  CompOperator getCompOp();

  /**
   * Sets the value of the '{@link CompExpression#getCompOp <em>Comp Op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Comp Op</em>' attribute.
   * @see CompOperator
   * @see #getCompOp()
   * @generated
   */
  void setCompOp(CompOperator value);

} // CompExpression
